import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state : {
        member : [],
        members : [],
        token : [],
        thread : [],
    },
    mutations : {
        //paramétrage des variables à stocker dans le store
        setMember(state, member) {
            state.member = member;
        },
        setMembers(state, members) {
            state.members = members;
        },
        setToken(state, token) {
            state.token = token;
        },
        setThread(state, thread) {
            state.thread = thread;
        },
        initialiseStore(state) {
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }
    }
})