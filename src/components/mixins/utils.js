export const Utils = {
  data() {
    return {
    messages: [],
    backgroundUrl:require('@/assets/bluemotif.gif'),
    }
  },
  methods : {
    //Retourne une image auto-génerée d'un avatar en fonction de l'email grâce au site adorable.io
    avatar(email){
      return 'https://api.adorable.io/avatars/285/' + email + '.png';
    },
      //Retourne les informations du membre connécté par rapport à la liste des membres
      getMember(id){
        let returnMember = false;
        this.$store.state.members.forEach((member) => {
            if(member._id == id)  {
              returnMember = member;
              return;
            }
        });
        return returnMember;
      },
      //Retourne les messages d'une chaine
      getMessages(messages){
        return messages;
      },
      //Permet de vérifier si le membre existe et s'il est bien connécté
      memberConnected(){
          if (this.$store.state.member.connected !== true){
              return false;
          }
          else{
              this.setTokenAxios(this.$store.state.token);
              return true;
          }
      },
      //Permet d'avoir le jeton d'identification du membre connécté
      setTokenAxios(token){
        window.axios.defaults.params.token = token;
      },
      //Retourne l'id de la discussion
      idDiscussion(){
          return this.$store.state.thread._id;
      },
      //Retourne le titre de la discussion
      labelDiscussion(){
          return this.$store.state.thread.label;
      },
      //Retourne la description de la discussion
      topicDiscussion(){
          return this.$store.state.thread.topic;
      },
      //Retourne la liste des membres
      setMembers(){
          return this.$store.state.members;
      },
  }
}