import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import AccountCreation from '@/components/AccountCreation'
import Dashboard from '@/components/Dashboard'
import SeeMembers from '@/components/SeeMembers'
import Discussion from '@/components/Discussion'
import SeePosts from '@/components/SeePosts'
import Member from '@/components/Member'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/AccountCreation',
      name: 'AccountCreation',
      component: AccountCreation
    },
    {
      path: '/Dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/SeeMembers',
      name: 'SeeMembers',
      component: SeeMembers
    },
    {
      path: '/Discussion/:id',
      name: 'Discussion',
      component: Discussion
    },
    {
      path: '/SeePosts',
      name: 'SeePosts',
      component: SeePosts
    },
    {
      path: '/Member/:id',
      name: 'Member',
      component: Member
    },
  ]
})
