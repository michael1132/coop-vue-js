/* 
LP CIASIE : Dev. Applications Web / Client
A l'intention de Gilles Francois : gilles.francois@sopress.net
Rendu TD : TD Co'op / Environnement de développement VueJS
Michael Franiatte : michael.franiatte@gmail.com
*/

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import store from './store'
import {Utils} from './components/mixins/utils.js';

Vue.config.productionTip = false

Vue.mixin(Utils);

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});

window.axios = axios.create({ baseURL: 'http://coop.api.netlor.fr/api', params : { token : false }, 
headers: { Authorization: 'Token token=f0b503d2ecf64c15a9ec1b2e89150bea' } });

/* eslint-disable no-new */
new Vue({

  mounted(){

    if (!this.memberConnected()) {
      this.$router.push('/');
    }
  },
  
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  beforeCreate() {
    this.$store.commit('initialiseStore');
  },
})
